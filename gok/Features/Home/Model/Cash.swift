//
//  Cash.swift
//  gok
//
//  Created by Albert Antonio Santos Oliveira - AOL on 19/11/20.
//

import Foundation

class Cash: Codable {
    var title: String?
    var bannerURL: String?
    var description: String?
}
