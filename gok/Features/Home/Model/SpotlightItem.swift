//
//  SpotlightItem.swift
//  gok
//
//  Created by Albert Antonio Santos Oliveira - AOL on 19/11/20.
//

import Foundation

class SpotlightItem: Codable {
    var name: String?
    var bannerURL: String?
    var description: String?
}
