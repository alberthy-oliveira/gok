//
//  Product.swift
//  gok
//
//  Created by Albert Antonio Santos Oliveira - AOL on 19/11/20.
//

import Foundation

class Product: Codable {
    var name: String?
    var imageURL: String?
    var description: String?
}
