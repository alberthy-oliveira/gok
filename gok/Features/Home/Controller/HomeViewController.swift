//
//  HomeViewController.swift
//  gok
//
//  Created by Albert Antonio Santos Oliveira - AOL on 18/11/20.
//

import UIKit
import SDWebImage

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    private let viewModel = HomeViewModel()
    private var cash: Cash?
    @IBOutlet weak var collectionViewSpotlight: UICollectionView!
    @IBOutlet weak var collectionViewProduct: UICollectionView!
    @IBOutlet weak var imageViewCash: UIImageView!
    @IBOutlet weak var titleCash: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.updatedData = {
            DispatchQueue.main.async {
                self.collectionViewSpotlight.reloadData()
                self.collectionViewProduct.reloadData()
                self.bindCash()
            }
        }
    }
    
    func bindCash() {
        self.titleCash.text = viewModel.getCash().title ?? ""
        let image = StringHelper.placeHolderImage
        self.imageViewCash.sd_setImage(with: URL(string: viewModel.getCash().bannerURL ?? ""),
                                       placeholderImage: UIImage(named: image))
        self.imageViewCash.layer.cornerRadius = 10
    }
    
}

extension HomeViewController {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewSpotlight {
            return viewModel.rowsOfSpotlight()
        }else {
            return viewModel.rowsOfProduct()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewSpotlight {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryboardHelper.spotlightCell, for: indexPath) as! SpotLightCollectionCell
            let cellVM = self.viewModel.cellSpotlightVM(forIndex: indexPath.row)
            cell.setup(viewModel: cellVM)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryboardHelper.productCell, for: indexPath) as! ProductCollectionCell
            let cellVM = self.viewModel.cellProductVM(forIndex: indexPath.row)
            cell.setup(viewModel: cellVM)
            return cell
        }
    }
    
}

class SpotLightCollectionCell: UICollectionViewCell {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var picture: UIImageView!
    func setup(viewModel: SpotlightCell) {
        let image = StringHelper.placeHolderImage
        picture.sd_setImage(with: URL(string: viewModel.getBanner()),
                            placeholderImage: UIImage(named: image))
        picture.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 8.0
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 10
    }
}
    
class ProductCollectionCell: UICollectionViewCell {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var picture: UIImageView!
    func setup(viewModel: ProductCell) {
        let image = StringHelper.placeHolderImage
        picture.sd_setImage(with: URL(string: viewModel.getImage()),
                            placeholderImage: UIImage(named: image))
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 8.0
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 20
    }
}


