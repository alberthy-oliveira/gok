//
//  ResponseHome.swift
//  gok
//
//  Created by Albert Antonio Santos Oliveira - AOL on 19/11/20.
//

import Foundation

class ResponseHome: Decodable {
    
    var spotlight: [SpotlightItem]
    var cash: Cash?
    var products: [Product]
    
}
