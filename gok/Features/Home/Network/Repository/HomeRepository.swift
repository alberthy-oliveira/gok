//
//  HomeRepository.swift
//  gok
//
//  Created by Albert Antonio Santos Oliveira - AOL on 19/11/20.
//

import Alamofire

class HomeRepository {
    
    let path: String = Bundle.main.path(forResource: "api", ofType: "plist")!
    let connection: NSDictionary
    
    var headers: HTTPHeaders
    var api: String
    
    init() {
        connection = NSDictionary(contentsOfFile: path)!
        headers = [
            "Content-Type": connection.object(forKey: "CONTENT_TYPE") as! String
        ]
        api = connection.object(forKey: "BASE_URL") as! String
    }
    
    func getData() -> DataRequest {
        return AF.request("\(api)/products", method: .get, headers: headers)
    }
    
}
