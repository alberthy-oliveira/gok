//
//  StoryboardHelper.swift
//  gok
//
//  Created by Albert Antonio Santos Oliveira - AOL on 19/11/20.
//

import Foundation

struct StoryboardHelper {
    static let spotlightCell = "SPOTLIGHT_CELL"
    static let productCell = "PRODUCT_CELL"
}
