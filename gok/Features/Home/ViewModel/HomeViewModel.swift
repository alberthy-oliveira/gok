//
//  HomeViewModel.swift
//  gok
//
//  Created by Albert Antonio Santos Oliveira - AOL on 18/11/20.
//

import Alamofire

public typealias UpdatedClosure = () -> ()

class HomeViewModel {
    
    public var updatedData: UpdatedClosure?
    private var cash: Cash?
    private let homeRepository = HomeRepository()
    
    private var spotlightData: [SpotlightCell] = [] {
        didSet {
            DispatchQueue.main.async {
                self.updatedData?()
            }
        }
    }

    private var productData: [ProductCell] = []
    
    public func tryFetchData() {
        self.homeRepository.getData().responseData { (response) in
            guard response.result.isSuccess else {
                return
            }
            let decoder = JSONDecoder()
            let responseSuccess: Swift.Result<ResponseHome, Error> = decoder.decodeResponse(from: response)
            let responseData = try! responseSuccess.get()
            self.bind(data: responseData)
        }
    }
    
    func bind(data: ResponseHome){
        self.cash = data.cash
        data.spotlight.forEach { (item) in
            self.spotlightData.append(SpotlightCell(item))
        }
        data.products.forEach { (product) in
            self.productData.append(ProductCell(product))
        }
    }
    
    public func rowsOfSpotlight() -> Int {
        let rows = self.spotlightData.count
        if rows == 0 {
            self.tryFetchData()
        }
        return self.spotlightData.count
    }
    
    public func cellSpotlightVM(forIndex index: Int) -> SpotlightCell {
        return self.spotlightData[index]
    }
    
    public func rowsOfProduct() -> Int {
        return self.productData.count
    }

    public func cellProductVM(forIndex index: Int) -> ProductCell {
        return self.productData[index]
    }
    
    func getCash() -> Cash {
        return self.cash ?? Cash()
    }

}

class SpotlightCell {
    private let spotlightItem: SpotlightItem?
        
    init(_ spotlightItem: SpotlightItem) {
        self.spotlightItem = spotlightItem
    }

    func getBanner() -> String {
        return self.spotlightItem!.bannerURL ?? ""
    }
}

class ProductCell {
    private let product: Product?

    init(_ product: Product) {
        self.product = product
    }

    func getImage() -> String {
        return self.product!.imageURL ?? ""
    }
}

extension JSONDecoder {
    func decodeResponse<T: Decodable>(from response: DataResponse<Data>) -> Swift.Result<T, Error> {
        
        guard response.error == nil else {
            print(response.error!)
            return .failure(response.error!)
        }
        
        guard let responseData = response.data else {
            return .failure(response.error!)
        }
        
        do {
            let item = try decode(T.self, from: responseData)
            return .success(item)
        } catch {
            print(error)
            return .failure(error)
        }
    }
}

